// Core
import axios from 'axios';
import { rootApiUrl } from './config';


import { ILoginToken } from '../types';

export const api = Object.freeze({
    auth: {
        async login(credentials: string): Promise<ILoginToken> {
            const { data } = await axios.get<ILoginToken>(`${rootApiUrl}/login`,
                {
                    headers: {
                        Authorization: `Basic ${credentials}`,
                    },
                });

            return data;
        },
    },
});
