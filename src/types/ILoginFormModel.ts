export interface ILoginFormModel {
    email: string;
    password: string;
}

export interface ILoginToken {
    data: string;
}
