import { FC, ReactElement } from 'react';
import cx from 'classnames';


// Styles
import Styles from './styles/index.module.scss';


interface IPropTypes {
    children: ReactElement | ReactElement[];
    center?: boolean;
    disabledWidget?: boolean;
}

export const Base: FC<IPropTypes> = (props) => {
    const {
        children,
        center,
        disabledWidget,
    } = props;

    // TODO; дані з сервера
    const score = 50;

    // TODO; динамічно міняти в залежності від вибраного поля при реєстрації
    const avatarCX = cx([
        Styles.sidebar, {
            [ Styles.male ]:   true,
            [ Styles.female ]: false,
        },
    ]);

    const contentCX = cx(Styles.content, {
        [ Styles.center ]: center,
    });

    const isWidgetDisplayed = score !== null && !disabledWidget;
    const widgetJSX = isWidgetDisplayed && (
        <div className = { Styles.widget }>
            <span className = { Styles.title }>Life Score</span>
            <div className = { Styles.module }>
                <span className = { Styles.score } style = { { bottom: `${score}%` } }>{ score }</span>
                <div className = { Styles.progress }>
                    <div className = { Styles.fill } style = { { height: `${score}%` } } />
                </div>
                <span className = { cx([Styles.label, Styles.level1]) }>Off Track</span>
                <span className = { cx([Styles.label, Styles.level2]) }>Imbalanced</span>
                <span className = { cx([Styles.label, Styles.level3]) }>Balanced</span>
                <span className = { cx([Styles.label, Styles.level4]) }>Healthy</span>
                <span className = { cx([Styles.label, Styles.level5]) }>Perfect Fit</span>
            </div>
        </div>
    );


    return (
        <section className = { Styles.profile }>
            <div className = { avatarCX }>
                { /* { loaderCX } */ }
            </div>
            <div className = { Styles.wrap }>
                <div className = { Styles.header }>
                    <h1>Profile component</h1>
                </div>
                <div className = { contentCX }>
                    { children }
                    { widgetJSX }
                </div>
            </div>
        </section>
    );
};

