import { UseFormRegisterReturn } from 'react-hook-form';

interface IPropTypes {
    placeholder?: string;
    className?: string
    type?: string;
    label?: string;
    register?: UseFormRegisterReturn;
    error?: {
        message?: string;
    };
}

export const Input: React.FC<IPropTypes> = (props) => {
    const input = (
        <input
            placeholder = { props.placeholder }
            type = { props.type }
            { ...props.register } />
    );

    return (
        <>
            <div className = { props.className } >
                <label>{ props.label }</label>
                { input }
            </div>
            <span>{ props.error?.message }</span>
        </>
    );
};
