// Components
import { RegistrationForm } from './RegistrationForm';

// Styles
import Styles from './styles/index.module.scss';


export const Registration: React.FC = () => {
    return (
        <section className = { Styles.registration }>
            <RegistrationForm />
        </section>
    );
};
