// Styles
import StylesForm from '../../theme/styles/Form.module.scss';

export const RegistrationFormActions: React.FC = () => {
    return (
        <div className = { StylesForm.controls }>
            <button className = { StylesForm.clearData }>Скинути</button>
            <button>Зареєструватись</button>
        </div>
    );
};
