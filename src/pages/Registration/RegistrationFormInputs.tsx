// Components
import { Input } from '../../components/elements/Input';
import StylesForm from '../../theme/styles/Form.module.scss';

export const RegistrationFormInputs: React.FC = () => {
    const formFields = [
        {
            type:        'email',
            label:       'Електронна пошта',
            placeholder: 'Введіть свою електронну пошту',
        },
        {
            type:        'text',
            label:       'Ім`я',
            placeholder: 'Введіть своє ім`я',
        },
        {
            type:        'text',
            label:       'Прізвище',
            placeholder: 'Введіть своє прізвище',
        },
        {
            type:        'password',
            label:       'Пароль',
            placeholder: 'Введіть свій пароль',
        },
        {
            type:        'number',
            label:       'Вік',
            placeholder: 'Введіть свій вік',
        },
        {
            type:        'number',
            label:       'Ріст',
            placeholder: 'Введіть свій ріс',
        },
        {
            type:        'number',
            label:       'Вага',
            placeholder: 'Введіть свою вагу',
        },
    ];

    return (
        <>
            { formFields.map((input) => <Input
                key = { input.label } className = { StylesForm.inputRow }
                label = { input.label } type = { input.type }
                placeholder = { input.placeholder } />)
            }
        </>
    );
};
