// Components
import { RegistrationFormInputs } from './RegistrationFormInputs';
import { RegistrationFormProfile } from './RegistrationFormProfile';
import { RegistrationFormActions } from './RegistrationFormActions';

// Styles
import Styles from './styles/index.module.scss';
import StylesProfile from '../../theme/styles/Profile.module.scss';


export const RegistrationForm: React.FC = () => {
    return (
        <>
            <div className = { Styles.left }>
                <form className = { StylesProfile.profile }>
                    <RegistrationFormProfile />
                    <RegistrationFormInputs />
                    <RegistrationFormActions />
                </form>
            </div>
            <div className = { Styles.right }></div>
        </>
    );
};
