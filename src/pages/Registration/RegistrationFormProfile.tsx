// Styles
import StylesProfile from '../../theme/styles/Profile.module.scss';

export const RegistrationFormProfile: React.FC = () => {
    return (
        <>
            <h1>Профіль</h1>
            <div className = { StylesProfile.gender }>
                <div className = { StylesProfile.female }>
                    <span>Жінка</span>
                </div>
                <div className = { StylesProfile.male }>
                    <span>Чоловік</span>
                </div>
            </div>
        </>
    );
};
