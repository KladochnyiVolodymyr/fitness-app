import * as yup from 'yup';
import { ILoginFormModel } from '../../types';

/* eslint-disable */
const tooShortMessage = 'мінімальна довжина - ${min} символів';
const tooLongMessage = 'максимальна довжина - ${max} символів';
const requiredMessage = 'поле обов\'язкове до заповнення';
/* eslint-enable */

export const schema: yup.SchemaOf<ILoginFormModel> = yup.object().shape({
    email: yup
        .string()
        .email()
        .required(requiredMessage),
    password: yup
        .string()
        .min(8, tooShortMessage)
        .max(16, tooLongMessage)
        .required(requiredMessage),
});
