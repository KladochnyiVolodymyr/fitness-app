import { Link, useNavigate } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { yupResolver } from '@hookform/resolvers/yup';
import { api } from '../../api';
import { Input } from '../../components/elements/Input';
import { ILoginFormModel } from '../../types';
import { schema } from './config';
import { authActions } from '../../lib/redux/actions/auth';

import StylesPage from './styles/index.module.scss';
import StylesForm from '../../theme/styles/Form.module.scss';


export const LoginForm: React.FC = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const form = useForm<ILoginFormModel>({
        mode:     'onTouched',
        resolver: yupResolver(schema),
    });

    const onSubmit = form.handleSubmit(async (credentials : ILoginFormModel) => {
        const baseFormat = window.btoa(`${credentials.email}:${credentials.password}`);
        try {
            const result = await api.auth.login(baseFormat);
            const token = result.data;

            dispatch(authActions.setToken(token));
            localStorage.setItem('token', token);
            navigate('/dashboard');
            form.reset();
        } catch {
            console.log('error');
        }
    });

    return (
        <form className = { StylesPage.content } onSubmit = { onSubmit }>
            <h1>Ласкаво просимо!</h1>
            <Input
                className = { StylesForm.inputRow }
                register = { form.register('email') }
                error = { form.formState.errors.email }
                type = 'email'
                label = 'Електронна пошта'
                placeholder = 'Введіть свою електронну пошту' />
            <Input
                className = { StylesForm.inputRow }
                register = { form.register('password') }
                error = { form.formState.errors.password }
                type = 'password'
                label = 'Пароль'
                placeholder = 'Введіть свій пароль' />
            <div>
                <button>Увійти до системи</button>
                <div className = { StylesPage.loginLink }>
                    <span>Якщо у вас немає облікового запису, будь ласка,</span>
                    <Link to = '/registration'> зареєструйтесь</Link>
                </div>
            </div>
        </form>
    );
};
