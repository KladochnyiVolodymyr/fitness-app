// Components
import { LoginForm } from './LoginForm';

// Styles
import Styles from './styles/index.module.scss';

export const Login: React.FC = () => {
    return (
        <section className = { Styles.login }>
            <LoginForm />
        </section>
    );
};
