import { FC } from 'react';
import {
    Route, Routes,
} from 'react-router-dom';


import { Login } from './pages/Login';
import { Registration } from './pages/Registration';
import { Dashboard } from './pages/Dashboard';


export const App: FC = () => {
    return (
        <>
            <Routes>
                <Route path = '/dashboard' element = { <Dashboard /> } />
                <Route path = '/login' element = { <Login /> } />
                <Route path = '/registration' element = { <Registration /> } />
            </Routes>
        </>
    );
};

