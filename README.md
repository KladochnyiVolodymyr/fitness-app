
1. 

```sh
yarn
# или
npm i
```

2. 

```sh
yarn dev
# или
npm run dev
```

3.

```sh
yarn build
# или
npm run build
```

4.

```sh
yarn build && yarn start  
# или
npm run build && npm run start
```
